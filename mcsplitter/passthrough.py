import asyncio


async def _writeTo(s_in, s_out, rest:bytes):
	if rest:
		s_out.write(rest)
		await s_out.drain()
	while True:
		tmp = await s_in.read(8192)
		if not tmp:
			break
		s_out.write(tmp)
		await s_out.drain()


async def passthrough(s1_in, s2_in, s1_out, s2_out, s1_rest:bytes, s2_rest:bytes):
	d, p = await asyncio.wait([_writeTo(s1_in, s2_out, s2_rest), _writeTo(s2_in, s1_out, s1_rest)], return_when=asyncio.FIRST_COMPLETED)
	for i in p:
		i.cancel()
		try:
			await i
		except asyncio.CancelledError:
			pass
	for i in d:
		await i
