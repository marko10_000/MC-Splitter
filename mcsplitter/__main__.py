import argparse
import asyncio
import json
import signal
from mcsplitter import splitter


_license = """MC-Splitter allows to manage connections depending on the connection DNS.
Copyright (C) 2022 Marko Semet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>."""

# Argument parse
args = argparse.ArgumentParser(epilog=_license, formatter_class=argparse.RawTextHelpFormatter)
args.add_argument("--bind", nargs="?", default="*", help="IP to bind to")
args.add_argument("--port", type=int, nargs="?", default=25565, help="Port to bind this service")
args.add_argument("config", nargs=1, help="Config file (json) to load")
conf = args.parse_args()

# Load config
def load_config(content:str):
	result = {}
	for iID, i in json.loads(content).items():
		host, port = i
		result[iID] = splitter.Entry(iID, host, port)
	splitter.entries = result

def _reload_config(file:str):
	load_config(open(file, "r").read())
	print("Reloaded configuration")

# Main loop
async def main_loop():
	load_config(open(conf.config[0], "r").read())
	asyncio.get_event_loop().add_signal_handler(signal.SIGUSR1, lambda: (_reload_config(conf.config[0])))
	await splitter.run(conf.bind, conf.port)

asyncio.run(main_loop())
