import asyncio
import dataclasses
from mcsplitter import passthrough
import os
import socket
import typing


@dataclasses.dataclass
class Entry:
	dns:str
	host:str
	port:int

	async def connect(self):
		return await asyncio.open_connection(self.host, self.port)


entries: typing.Dict[str, Entry] = {}


def _varInteger(content: bytes, maxLength: int):
	res = 0
	mov = 0
	for i in content:
		res |= (i & 0x7F) << (7 * mov)
		mov += 1
		if (i & 0x80) == 0:
			break
		if mov == maxLength:
			raise ValueError("VarInteger too long.")
	return (mov, res)


def _string(content: bytes, maxLength: int):
	pos, size = _varInteger(content, 3)
	res = content[pos:pos + size].decode("UTF-8")
	if len(res) > maxLength:
		raise ValueError("String too long")
	return (pos + size, res)


async def _loadPackage(stream, buffer:bytes):
	if len(buffer) < 5:
		buffer += await stream.read(5)
	(pos, length) = _varInteger(buffer, 5)
	if len(buffer) - pos < length:
		buffer += await stream.read(length - (len(buffer) - pos))
	if len(buffer) < pos + length:
		raise ValueError("%i %i" % (pos + length, len(buffer)));
	(_pos, pid) = _varInteger(buffer[pos:], 5)
	pos += _pos
	return buffer, pid, buffer[pos:pos + length - _pos], buffer[pos + length - _pos:]

async def _loadUTF16(stream, buffer:bytes):
	if len(buffer) < 2:
		buffer += await stream.readexactly(2 - len(buffer))
	length = (buffer[0] << 8) | buffer[1]
	if len(buffer) - 2 < length * 2:
		buffer += await stream.readexactly(length * 2 - (len(buffer) - 2))
	return buffer, buffer[2:length * 2 + 2].decode("UTF-16BE"), buffer[length * 2 + 2:]

async def _loadContent(stream, buffer: bytes):
	if len(buffer) < 2:
		buffer += await stream.readexactly(2 - len(buffer))
	length = (buffer[0] << 8) | buffer[1]
	if len(buffer) - 2 < length:
		buffer += await stream.readexactly(length - (len(buffer) - 2))
	return buffer, buffer[2:length + 2], buffer[length + 2:]

async def split(s_in, s_out):
	s = s_out.get_extra_info("socket")
	print("Incomming connection %s:%i" % s.getpeername())
	try:
		ping_test = await s_in.readexactly(3)
		if ping_test == b"\xFE\x01\xFA":
			buf1, _, nextb = await _loadUTF16(s_in, b"")
			buf2, con, nextb = await _loadContent(s_in, nextb)
			_, dns, _ = await _loadUTF16(None, con[1:])
			buffer = b"\xFE\x01\xFA" + buf1 + buf2
		else:
			buffer, _, con_data, _ = await _loadPackage(s_in, ping_test)
			pos, _ = _varInteger(con_data, 4)
			_, dns = _string(con_data[pos:], 255)
		if dns in entries:
			s2_in, s2_out = await entries[dns].connect()
			try:
				print("Connect to %s ==> %s:%i" % (dns, entries[dns].host, entries[dns].port))
				await passthrough.passthrough(s_in, s2_in, s_out, s2_out, b"", buffer)
			finally:
				s2_out.close()
				await s2_out.wait_closed()
		else:
			print("Rejected dns %s" % dns)
	except Exception as e:
		print(repr(e))
		raise
	finally:
		print("Connection lost from %s:%i" % s.getpeername())
		s_out.close()
		await s_out.wait_closed()


def _systemdReady():
	path = os.environ.get("NOTIFY_SOCKET", None)
	if path is None:
		return
	try:
		sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
		sock.connect(path)
		sock.setsockopt(socket.SOL_SOCKET, socket.SO_PASSCRED, 1)
		sock.send(b"READY=1\n")
	except Exception as e:
		print("Can't notify systemd.")
		print(repr(e))


async def run(host:str, port:int):
	print("Bind port %s:%i" % (host, port))
	server = await asyncio.start_server(split, host=host, port=port)
	_systemdReady()
	await server.serve_forever()
